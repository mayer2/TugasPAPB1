package com.example.tugaspapb1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val namaTextView: TextView = findViewById(R.id.namaTextView)
        val nimTextView: TextView = findViewById(R.id.nimTextView)

        val nama = "Mayer Omega Parlindungan"
        val nim = "215150400111045"

        namaTextView.text = "Nama: $nama"
        nimTextView.text = "NIM: $nim"
    }
}